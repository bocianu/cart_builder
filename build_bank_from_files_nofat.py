import sys
import os
import glob
infilesmask = sys.argv[1]
startbank = int(sys.argv[2])
outname = 'bank_'
outext = '.bnk'
#insize  = os.stat(infile).st_size
outsize = 8192
addr_offset = 0xA000

infiles = glob.glob(infilesmask)
infiles.sort()
banks = []
bank = startbank

blocksize = 0;
outblock = [0xff] * outsize

def newBank():
    global bank, outblock, blocksize
    print('*** BUILDING BLOCK '+ str(bank) +' ***')
    outfile = outname + str(bank) + outext
    with open(outfile, 'wb') as out_file:
        out_file.write(bytearray(outblock))
    blocksize = 0;
    outblock = [0xff] * outsize
    bank += 1
    
def fatEntry(entry):
    return [
        entry[0] & 0xff,               
        (entry[0] & 0xff00) >> 8,
        entry[1] & 0xff,               
        (entry[1] & 0xff00) >> 8,
        entry[2],
        0,0,0
    ]

while len(infiles) > 0:
    thefile = infiles.pop(0)
    fsize = os.stat(thefile).st_size
    if blocksize + fsize > outsize:
        newBank()
    with open(thefile, 'rb') as in_file:
        indata = in_file.read()
        outblock[blocksize:blocksize+fsize] = indata
        blocksize += fsize
        
if blocksize > 0:
    newBank();
    



