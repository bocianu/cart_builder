#!/bin/bash
BOOT_NAME=bootloader
LOADER_NAME=xexloader
GAME_NAME=$1
LOADER_ORG=9000
VERSION_NUMBER=`cat ../vnumber.inc | grep 'VERSION_M' | sed 's/[^0-9]*//g' | tr '\n' ' '`

# building loaders
./clean.bat $BOOT_NAME.xex $LOADER_NAME.xex
./build_loader.bat $BOOT_NAME a000 0600
./build_loader.bat $LOADER_NAME $LOADER_ORG
python trim_boot_loader.py $BOOT_NAME.xex $BOOT_NAME.bin

# and build bank 0
python build_bank_0.py $BOOT_NAME.bin $LOADER_NAME.xex $VERSION_NUMBER

# merge intro and put main executable from bank 1++
./merge_intro.sh $GAME_NAME start.xex
python build_bank_from_file.py _$GAME_NAME 1

# store credits
python build_bank_from_files.py '../assets/credits/credits*.apu' 7

# store intro
python build_bank_from_files.py '../assets/intro/intro_*.apu' 15

# store intro
python build_bank_from_files.py '../assets/intro/outro_*.apu' 23


# build worlds
python build_bank_from_file.py '../worlds/00_Escape_From_The_Lab/world_00.bin' 8
python build_bank_from_file.py '../worlds/00_Escape_From_The_Lab/world_00_assets.bin' 9
python build_bank_from_files.py '../worlds/00_Escape_From_The_Lab/room_*.lz4' 10

python build_bank_from_file.py '../worlds/01_Abominable_Sewers/world_01.bin' 16
python build_bank_from_file.py '../worlds/01_Abominable_Sewers/world_01_assets.bin' 17
python build_bank_from_files.py '../worlds/01_Abominable_Sewers/room_*.lz4' 18

python build_bank_from_file.py '../worlds/02_Suburbian_Parkour/world_02.bin' 24
python build_bank_from_file.py '../worlds/02_Suburbian_Parkour/world_02_assets.bin' 25
python build_bank_from_files.py '../worlds/02_Suburbian_Parkour/room_*.lz4' 26

python build_bank_from_file.py '../worlds/03_Hazardous_Mudlands/world_03.bin' 32
python build_bank_from_file.py '../worlds/03_Hazardous_Mudlands/world_03_assets.bin' 33
python build_bank_from_files.py '../worlds/03_Hazardous_Mudlands/room_*.lz4' 34

python build_bank_from_file.py '../worlds/04_Silver_Dust_Mines/world_04.bin' 40
python build_bank_from_file.py '../worlds/04_Silver_Dust_Mines/world_04_assets.bin' 41
python build_bank_from_files.py '../worlds/04_Silver_Dust_Mines/room_*.lz4' 42

python build_bank_from_file.py '../worlds/05_Return_To_The_Lab/world_05.bin' 48
python build_bank_from_file.py '../worlds/05_Return_To_The_Lab/world_05_assets.bin' 49
python build_bank_from_files.py '../worlds/05_Return_To_The_Lab/room_*.lz4' 50

#python build_bank_from_files_nofat.py './save_*.sav' 56

python build_cart.py
 

