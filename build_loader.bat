@echo off
setlocal
for /f "delims=" %%x in (../buildconfig.conf) do (set "%%x")
@echo on
IF "%3"=="" (
    %MP_EXE% -code:%2 %1.pas
) ELSE (
    %MP_EXE% -code:%2 -data:%3 %1.pas
)
%MADS_EXE% %1.a65 -x -i:E:\atari\MadPascal\base -o:%1.xex

ataricom.exe %1.xex | grep RUN > %1.runadr

