import sys
import os

if len(sys.argv) < 6:
    print('too few arguments')
    exit()

loaderfile =  sys.argv[1]
xexfile = sys.argv[2]
ver_main = int(sys.argv[3])
ver_major = int(sys.argv[4])
ver_minor = int(sys.argv[5])

if not os.path.isfile(loaderfile):
    print('File: '+loaderfile+' does not exist')
    exit()

if not os.path.isfile(xexfile):
    print('File: '+xexfile+' does not exist')
    exit()

def getRunAdr(fname):
    filename = os.path.splitext(fname)[0]+'.runadr'
    if not os.path.isfile(filename):
        print('File: '+filename+' does not exist')
        exit()
    with open(filename, 'r') as in_file:
        runstr = in_file.read()
    hexaddr = runstr.strip().split(' ')[1];
    return int(hexaddr, 16)
    
outfile1 = 'bank_0.bnk'
outfile2 = 'bank_127.bnk'
outsize = 8192

outblock = [0xff] * outsize
constz = ''

def uHex(val):
    return f'${hex(val).lstrip("0x")}'

def appendFile(filename, start, header = None):
    with open(filename, 'rb') as in_file:
        global constz
        basename = os.path.basename(filename);
        indata = in_file.read()
        offset = start;
        if header is not None:
            outblock[offset:offset+len(header)] = header
            offset += len(header);
        end = offset + len(indata)            
        outblock[offset:end] = indata
        print(f'File "{basename}" {uHex(start)}->{uHex(end-1)}')
        
        constz = constz + f'B0_{basename.upper()} = {uHex(0xa000 + start)} ;\n'
        return end;
        

loader_runadr = getRunAdr(loaderfile)
xex_runadr = getRunAdr(xexfile)

appendFile(loaderfile, 0);
appendFile(xexfile, 0x180);

next = appendFile('../assets/tgStudio.apu', 0x400);
next = appendFile('../assets/mqlogo.apu', next);
next = appendFile('../assets/flob_title.apu', next);
next = appendFile('../assets/bigmap.gfx', next, [8,40]);
next = appendFile('../assets/skull.gfx', next, [8,40]);
next = appendFile('../assets/bigvial.gfx', next, [8,40]);
next = appendFile('../assets/bigformula.gfx', next, [8,40]);
next = appendFile('../assets/cheat.gfx', next, [13,27]);
next = appendFile('../assets/statback.apu', next);
next = appendFile('../assets/flob_theme.apu', next);
next = appendFile('../assets/crown2.apu', next);

print (f'Storing version number: {ver_main}.{ver_major}.{ver_minor}')
outblock[outsize-9] = ver_main;
outblock[outsize-8] = ver_major;
outblock[outsize-7] = ver_minor;

#storing cart header

outblock[outsize-6]= xex_runadr & 0xff                #BFFA
outblock[outsize-5]= (xex_runadr & 0xff00) >> 8       #BFFB
outblock[outsize-4]= 0x00                               #BFFC
outblock[outsize-3]= 0x04                               #BFFD
outblock[outsize-2] = loader_runadr & 0xff            #BFFE
outblock[outsize-1] = (loader_runadr & 0xff00) >> 8   #BFFF

print('*** BUILDING BLOCK0 ***')
with open(outfile1, 'wb') as out_file:
    out_file.write(bytearray(outblock))
with open(outfile2, 'wb') as out_file:
    out_file.write(bytearray(outblock))
with open('bank_0_files.inc', 'w') as out_file:
    out_file.write(constz)
